﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MVCFile.DataSet;
using MVCnews.Models;
using System.Data;

namespace MVCnews.Controllers
{
    public class MemberController : Controller
    {
        private DSUser DB =  DSUser.Instance;
        // GET: Member

        public ActionResult Login()
        {            
            return View();
        }
        [HttpPost]
        public ActionResult Login(User user)
        {
            string pw = user.upw;
            SHA256 sha256 = new SHA256CryptoServiceProvider();//建立一個SHA256
            byte[] source = Encoding.Default.GetBytes(pw);//將字串轉為Byte[]
            byte[] crypto = sha256.ComputeHash(source);//進行SHA256加密
            string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
            DataTable dt = DB.MemberLogin(user.uaccount, result);
            if (dt.Rows.Count > 0)
            {
                Session["w_name"] = dt.Rows[0]["uname"];
                Session["w_id"] = dt.Rows[0]["uid"];
                TempData["message"] = "登入成功";
            }
            return View();
        }
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(User user)
        {            
            string pw = user.upw;
            string name = user.uname;
            string acc = user.uaccount;
            string mail = user.umail;
            SHA256 sha256 = new SHA256CryptoServiceProvider();//建立一個SHA256
            byte[] source = Encoding.Default.GetBytes(pw);//將字串轉為Byte[]
            byte[] crypto = sha256.ComputeHash(source);//進行SHA256加密
            string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
            bool rs =  DB.InsertMember(name, acc, pw, result, mail);
            //if (rs)
            //{                
            //    TempData["message"] = "歡迎加入會員，直接協助登入";
            //}
            //else
            //{
            //    TempData["message"] = "";
            //}
            return RedirectToAction("Login", "Member");
            //return RedirectToAction("Index", "Home");
        }
        public ActionResult Update()
        {
            if (Session["w_id"] != null)
            {
                int id = Convert.ToInt32(Session["w_id"]);
                DataTable dt = new DataTable();
                dt = DB.GetMemberDataByID(id);

                ChangeUser Model = new ChangeUser();

                foreach (DataRow dr in dt.Rows)
                {
                    Model.id = dr.Field<int>("uid");
                    Model.uaccount = dr.Field<string>("uaccount");
                    Model.uname = dr.Field<string>("uname");
                    Model.umail = dr.Field<string>("umail");
                }

                return View(Model);
            }else
            {
                return RedirectToAction("Login", "Member");

            }
        }
        [HttpPost]
        public ActionResult Update(ChangeUser user)
        {
            bool rs = false;
            //檢查ModelState是否異常
            
            int id = user.id;          
            string lpw = user.lastupw;          
            DataTable dt  = DB.CheckMemberPw(id, hashstring(lpw));
            if (dt.Rows.Count > 0)//有此會員密碼正確
            {
                string name = user.uname;
                string ac = user.uaccount;
                string pw = user.upw;
                string result = hashstring(pw);
                string umail = user.umail;
                rs = DB.UpdateMember(name, ac, pw, result, umail, id);
            }
            else
            {
                TempData["message"] = "密碼錯誤";
                return View();
            }

            if (rs)
            {
                TempData["message"] = "修改成功";
                return RedirectToAction("Index", "Home");
            }else
            {
                return View();
            }
          

            
        }
        public ActionResult LogOff()
        {
            Session["w_id"] = null;
            Session["w_name"] = null;
            return RedirectToAction("Login", "Member");
        }

        public string hashstring (string str)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();//建立一個SHA256
            byte[] source = Encoding.Default.GetBytes(str);//將字串轉為Byte[]
            byte[] crypto = sha256.ComputeHash(source);//進行SHA256加密
            string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串

            return result;
        }
    }
}
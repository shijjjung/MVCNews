﻿using MVCFile.DataSet;
using MVCnews.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCnews.Controllers
{
    public class FilesController : Controller
    {
        private DSFile DB = DSFile.Instance;
        // GET: Files
        public ActionResult Index()
        {
            DataTable dt = new DataTable();
            dt = DB.GetFileData();
            ViewData.Model = dt.AsEnumerable();
            return View();
        }
        [HttpPost]
        public void Store(nFile file)
        {
           
           
        }

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            List<nFile> filelist = new List<nFile>();
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        nFile obj = new nFile();
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                           
                              fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.
                        string filename = Path.Combine(Server.MapPath("~/Upload/"), fname);
                        obj.name = fname.Split('.')[0];
                        obj.extension = fname.Split('.')[1];
                        if (System.IO.File.Exists(filename))
                        {
                            int my_counter = 2;
                            string tempfilename = "";
                            //如果有就把相同檔名的檔案改掉,因為可能不只有一個檔名相同而已,所以用while相同都要改,然後數字會遞增去改
                            while (System.IO.File.Exists(filename))
                            {                                
                                tempfilename = fname.Split('.')[0] + my_counter +"."+ fname.Split('.')[1];
                                obj.name = fname.Split('.')[0]+ my_counter;
                                obj.extension = fname.Split('.')[1];                               
                                filename = Path.Combine(Server.MapPath("~/Upload/"), tempfilename);
                                my_counter += 1;
                            }
                            //把修改好的檔名丟給filename
                            fname = tempfilename;                           
                        }
                        obj.url = filename;
                        filelist.Add(obj);
                        file.SaveAs(filename);
                        DB.InsertFile(obj.name, obj.url, obj.extension);
                    }
                    // Returns message that successfully uploaded                      
                    return Json(filelist);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
    }
}
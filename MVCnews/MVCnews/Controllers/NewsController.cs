﻿
using MVCnews.DataSet;
using MVCnews.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCnews.Controllers
{
    
    public class NewsController : Controller
    {
        private DSNews newsDB = DSNews.Instance;
        // GET: News
        public ActionResult Index()
        {
            DataTable dt = new DataTable();
            dt = newsDB.GetnewsData();
            ViewData.Model = dt.AsEnumerable();
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Store(FormCollection collection)
        {
            var inputCount = 0; //前端文本框的数量
            var inputValues = new List<string>();//前端文本款的值放到这个集合

            if (int.TryParse(collection["TextBoxCount"], out inputCount))
            {
                for (int i = 1; i <= inputCount; i++)
                {
                    if (!string.IsNullOrEmpty(collection["textbox" + i]))
                    {
                       
                        bool e = newsDB.Insertnews(collection["textbox" + i].ToString());
                        
                    }
                }
            }
            return Redirect("Index");
          
        }
        
      
        public ActionResult Update(int id)
        {
            
            DataTable dt = new DataTable();
            dt = newsDB.GetnewsDataByID(id);

            News Model = new News();

            foreach (DataRow dr in dt.Rows)
            {

                Model.id = dr.Field<int>("id");
                Model.context = dr.Field<string>("context");
               
            }
            
            return View(Model);
        }
       
        [HttpPost]
        public ActionResult Update(News news)
        {
            bool rs = false;
            //檢查ModelState是否異常
           
            string c = news.context;
            int i = news.id;
            rs =  newsDB.Updatenews(c, i);
            if (rs)
                TempData["message"] = "修改成功";
            else
                TempData["message"] = "修改失敗";                
            
            return RedirectToAction("Index", "News");
          
        }
        public ActionResult Delete(int id)
        {
            bool rs = newsDB.Delnews(id);
            if (rs)
                TempData["message"] = "刪除成功";
            else
                TempData["message"] = "刪除失敗";

            return RedirectToAction("Index", "News");

        }

    }
}
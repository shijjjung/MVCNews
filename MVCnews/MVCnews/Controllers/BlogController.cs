﻿using MVCFile.DataSet;
using MVCnews.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCnews.Controllers
{
    public class BlogController : Controller
    {
        private DSBlog DB = DSBlog.Instance;
        // GET: Blog
        public ActionResult Index()
        {
            DataTable dt = new DataTable();
            dt = DB.GetBlogData();
          
            ViewData.Model = dt.AsEnumerable();
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Blog blog)
        {            
            string c = HttpUtility.HtmlDecode(blog.context);
            DB.InsertBlog(blog.title, blog.subtitle, c);
            return Redirect("Index");
        }


        public ActionResult Detail(int id )
        {
            Blog Model = new Blog();
            DataTable dt = DB.GetBlogDataByID(id);
            foreach (DataRow dr in dt.Rows)
            {
                Model.bid = dr.Field<int>("bid");
                Model.title = dr.Field<string>("title");
                Model.context = dr.Field<string>("context");
                Model.subtitle = dr.Field<string>("subtitle");
            }

            return View(Model);
          
            
        }
        [HttpPost]
        public ActionResult Update(Blog blog)
        {
            bool rs = false;
            //檢查ModelState是否異常

          
            string t = blog.title;
            string st = blog.subtitle;
            string c = HttpUtility.HtmlDecode(blog.context);
            int i = blog.bid;
            rs = DB.UpdateBlog(t, st, c, i);
            if (rs)
                TempData["message"] = "修改成功";
            else
                TempData["message"] = "修改失敗";

            return RedirectToAction("Index", "Blog");

        }
        public ActionResult Delete(int id)
        {
            bool rs = DB.DelBlog(id);
            if (rs)
                TempData["message"] = "刪除成功";
            else
                TempData["message"] = "刪除失敗";

            return RedirectToAction("Index", "Blog");

        }
    }
}
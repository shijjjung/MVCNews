﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCnews.Models
{
    public class nFile
    {
        public int fid { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string extension { get; set; }
        public int nid { get; set; }
        public DateTime created { get; set; }       
    }
}
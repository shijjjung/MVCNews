﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCnews.Models
{
    public class User
    {
        public int id { get; set; }                
        public string lastest_at { get; set; }      
        public DateTime created_at { get; set; }


        [Required]
        [Display(Name = "使用者名稱")]
        public string uname { get; set; }

        [Required]
        [Display(Name = "使用者帳號")]
        public string uaccount { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "使用者密碼")]
        public string upw { get; set; }



        [Required]
        [Display(Name = "信箱")]
        public string umail { get; set; }

    }
    public class ChangeUser
    {
        public int id { get; set; }
        public string lastest_at { get; set; }
        public DateTime created_at { get; set; }


        [Required]
        [Display(Name = "使用者名稱")]
        public string uname { get; set; }

        [Required]
        [Display(Name = "使用者帳號")]
        public string uaccount { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "新使用者密碼")]/*可以自動抓Name 當作Label名稱 可查看Member/Register or Update */
        public string upw { get; set; }


        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "原使用者密碼")]
        public string lastupw { get; set; }

        [Required]
        [Display(Name = "信箱")]
        public string umail { get; set; }

    }


}
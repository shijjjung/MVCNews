﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCnews.Models
{
    public class Blog
    {

        public int bid { get; set; }
        [Display(Name = "標題")]
        [Required(ErrorMessage = "標題 為必填")]
        public string title { get; set; }
      
      
        [Display(Name = "種類")]
        [Required(ErrorMessage = "種類 為必填")]
        public string subtitle { get; set; }
        [Display(Name = "內容")]
        [Required(ErrorMessage = "內容 為必填")]
        public string context { get; set; }
        public int goodcount { get; set; }
        public int badcount { get; set; }
        [Display(Name = "新增時間")]
        public DateTime created { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using MVCnews.DataSet.SQL;
using MVCnews.Models;

namespace MVCnews.DataSet
{
    public class DSNews
    {
        private MSSQL m_db = new MSSQL();

        private static readonly Lazy<DSNews> LazyInstance = new Lazy<DSNews>(() => new DSNews());
        private DSNews() { }
        public static DSNews Instance
        {
            get { return LazyInstance.Value; }
        }

        public DataTable GetnewsData()
        {
            string sql = @"SELECT *
                           FROM news
                           ORDER BY created";

            using (SqlCommand command = new SqlCommand(sql))
            {
                return this.m_db.Fill(command);
            }
        }      
       public DataTable GetnewsDataTop3()
        {
            string sql = @"SELECT  Top 3 *  FROM news  ORDER BY created desc";

            using (SqlCommand command = new SqlCommand(sql))
            {
                return this.m_db.Fill(command);
            }
        }

        public DataTable GetnewsDataByID(int news_id)
        {
            string sql = @"SELECT *
                           FROM news
                           WHERE id=@news_id ";

            using (SqlCommand command = new SqlCommand(sql))
            {
                command.Parameters.AddWithValue("news_id", news_id);
                return this.m_db.Fill(command);
            }
        }

        public bool Insertnews(string context)
        {
            try
            {               
                string sql = @"  INSERT INTO news(context,created)
                                    VALUES (@context, GETDATE());
                                    ";
                using (SqlCommand command = new SqlCommand(sql))
                {
                    command.Parameters.AddWithValue("context", context);
                    
                    m_db.ExecuteNonQuery(command);
                }
                  
                return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Updatenews(string context,  int news_id)
        {
            try
            {
                string sql = @" UPDATE news SET context=@context 
                                    WHERE id=@news_id;";
                using (SqlCommand command = new SqlCommand(sql))
                {
                    command.Parameters.AddWithValue("context", context);
                    command.Parameters.AddWithValue("news_id", news_id);              
                    m_db.ExecuteNonQuery(command);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }





        public bool Delnews(int uid)
        {
            try
            {            
                    string sql = @"DELETE FROM news
                                    WHERE id=@news_id;";
                    using (SqlCommand command = new SqlCommand(sql))
                    {
                        command.Parameters.AddWithValue("news_id", uid);
                        m_db.ExecuteNonQuery(command);
                    }
                    
                    return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
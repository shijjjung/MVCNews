﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

using MVCnews.DataSet.SQL;

namespace MVCFile.DataSet
{
    public class DSFile
    {
        private MSSQL m_db = new MSSQL();

        private static readonly Lazy<DSFile> LazyInstance = new Lazy<DSFile>(() => new DSFile());
        private DSFile() { }
        public static DSFile Instance
        {
            get { return LazyInstance.Value; }
        }

        public DataTable GetFileData()
        {
            string sql = @"SELECT *
                           FROM files
                           ORDER BY created";

            using (SqlCommand command = new SqlCommand(sql))
            {
                return this.m_db.Fill(command);
            }
        }      
       public DataTable GetFileDataTop3()
        {
            string sql = @"SELECT  Top 3 *  FROM files  ORDER BY created desc";

            using (SqlCommand command = new SqlCommand(sql))
            {
                return this.m_db.Fill(command);
            }
        }

        public DataTable GetFileDataByID(int File_id)
        {
            string sql = @"SELECT *
                           FROM files
                           WHERE fid=@File_id ";

            using (SqlCommand command = new SqlCommand(sql))
            {
                command.Parameters.AddWithValue("File_id", File_id);
                return this.m_db.Fill(command);
            }
        }

        public bool InsertFile(string name,string url , string extension )
        {
            try
            {               
                string sql = @"  INSERT INTO files(name,url,extension,created)
                                    VALUES (@name,@url,@extension, GETDATE());
                                    ";
                using (SqlCommand command = new SqlCommand(sql))
                {
                    command.Parameters.AddWithValue("name", name);
                    command.Parameters.AddWithValue("url", url);
                    command.Parameters.AddWithValue("extension", extension);
                   

                    m_db.ExecuteNonQuery(command);
                }
                  
                return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateFile(string name, string url, string extension, int nid,  int fid)
        {
            try
            {
                string sql = @" UPDATE files SET name=@name ,url=@url ,extension=@extension 
                                    WHERE fid=@fid;";
                using (SqlCommand command = new SqlCommand(sql))
                {
                    command.Parameters.AddWithValue("name", name);
                    command.Parameters.AddWithValue("url", url);
                    command.Parameters.AddWithValue("extension", extension);
                    
                    command.Parameters.AddWithValue("fid", fid);              
                    m_db.ExecuteNonQuery(command);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }





        public bool DelFile(int uid)
        {
            try
            {            
                    string sql = @"DELETE FROM files
                                    WHERE fid=@File_id;";
                    using (SqlCommand command = new SqlCommand(sql))
                    {
                        command.Parameters.AddWithValue("File_id", uid);
                        m_db.ExecuteNonQuery(command);
                    }
                    
                    return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
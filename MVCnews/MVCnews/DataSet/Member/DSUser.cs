﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

using MVCnews.DataSet.SQL;

namespace MVCFile.DataSet
{
    public class DSUser
    {
        private MSSQL m_db = new MSSQL();

        private static readonly Lazy<DSUser> LazyInstance = new Lazy<DSUser>(() => new DSUser());
        private DSUser() { }
        public static DSUser Instance
        {
            get { return LazyInstance.Value; }
        }

        public DataTable GetMemberData()
        {
            string sql = @"SELECT *
                           FROM users
                           ORDER BY created_at";

            using (SqlCommand command = new SqlCommand(sql))
            {
                return this.m_db.Fill(command);
            }
        }      
       public DataTable MemberLogin(string name , string hpw)
        {
            string sql = @"SELECT *  FROM users  where uname = @name and hashpw = @hpw";

            using (SqlCommand command = new SqlCommand(sql))
            {
                command.Parameters.AddWithValue("name", name);
                command.Parameters.AddWithValue("hpw", hpw);
                return this.m_db.Fill(command);
            }
        }

        public DataTable GetMemberDataByID(int Id)
        {
            string sql = @"SELECT *
                           FROM users
                           WHERE uid=@Id ";

            using (SqlCommand command = new SqlCommand(sql))
            {
                command.Parameters.AddWithValue("Id", Id);
                return this.m_db.Fill(command);
            }
        }
        public DataTable CheckMemberPw(int Id,string hpw)
        {
            string sql = @"SELECT *
                           FROM users
                           WHERE uid=@Id and hashpw = @hpw";

            using (SqlCommand command = new SqlCommand(sql))
            {
                command.Parameters.AddWithValue("Id", Id);
                command.Parameters.AddWithValue("hpw", hpw);
                return this.m_db.Fill(command);
            }
        }
        public bool InsertMember(string name,string ac, string pw,string hpw,string mail)
        {
            try
            {               
                string sql = @"INSERT INTO users(uname,uaccount,upw,created_at,hashpw,umail) 
                VALUES (@name,@ac,@pw,GETDATE(),@hpw,@mail)";
                using (SqlCommand command = new SqlCommand(sql))
                {
                    command.Parameters.AddWithValue("name", name);
                    command.Parameters.AddWithValue("ac", ac);
                    command.Parameters.AddWithValue("pw", pw);
                    command.Parameters.AddWithValue("hpw", hpw);
                    command.Parameters.AddWithValue("mail", mail);
                    m_db.ExecuteNonQuery(command);
                }
                  
                return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateMember(string name, string ac, string pw, string hpw, string mail, int Id)
        {
            try
            {
                string sql = @" UPDATE users SET uname=@name ,uaccount=@ac ,upw=@upw ,umail = @mail,hashpw=@hpw
                                    WHERE uid=@Id;";
                using (SqlCommand command = new SqlCommand(sql))
                {
                    command.Parameters.AddWithValue("name", name);
                    command.Parameters.AddWithValue("ac", ac);
                    command.Parameters.AddWithValue("upw", pw);
                    command.Parameters.AddWithValue("hpw", hpw);
                    command.Parameters.AddWithValue("mail", mail);
                    command.Parameters.AddWithValue("Id", Id);              
                    m_db.ExecuteNonQuery(command);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }





        //public bool DelMember(int uid)
        //{
        //    try
        //    {            
        //            string sql = @"DELETE FROM users
        //                            WHERE uid=@Id;";
        //            using (SqlCommand command = new SqlCommand(sql))
        //            {
        //                command.Parameters.AddWithValue("Id", uid);
        //                m_db.ExecuteNonQuery(command);
        //            }
                    
        //            return true;
                
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

    }
}
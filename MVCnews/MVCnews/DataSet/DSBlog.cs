﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

using MVCnews.DataSet.SQL;

namespace MVCFile.DataSet
{
    public class DSBlog
    {
        private MSSQL m_db = new MSSQL();

        private static readonly Lazy<DSBlog> LazyInstance = new Lazy<DSBlog>(() => new DSBlog());
        private DSBlog() { }
        public static DSBlog Instance
        {
            get { return LazyInstance.Value; }
        }

        public DataTable GetBlogData()
        {
            string sql = @"SELECT *
                           FROM blogs
                           ORDER BY created";

            using (SqlCommand command = new SqlCommand(sql))
            {
                return this.m_db.Fill(command);
            }
        }      
       public DataTable GetBlogDataTop3()
        {
            string sql = @"SELECT  Top 3 *  FROM blogs  ORDER BY created desc";

            using (SqlCommand command = new SqlCommand(sql))
            {
                return this.m_db.Fill(command);
            }
        }

        public DataTable GetBlogDataByID(int File_id)
        {
            string sql = @"SELECT *
                           FROM blogs
                           WHERE bid=@File_id ";

            using (SqlCommand command = new SqlCommand(sql))
            {
                command.Parameters.AddWithValue("File_id", File_id);
                return this.m_db.Fill(command);
            }
        }

        public bool InsertBlog(string title, string subtitle, string context)
        {
            try
            {               
                string sql = @"  INSERT INTO blogs(title,subtitle,context,created)
                                    VALUES (@title,@subtitle,@context, GETDATE());
                                    ";
                using (SqlCommand command = new SqlCommand(sql))
                {
                    command.Parameters.AddWithValue("title", title);
                    command.Parameters.AddWithValue("subtitle", subtitle);
                    command.Parameters.AddWithValue("context", context);
                   

                    m_db.ExecuteNonQuery(command);
                }
                  
                return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateBlog(string title, string subtitle, string context,  int bid)
        {
            try
            {
                string sql = @" UPDATE blogs SET title=@title ,subtitle=@subtitle ,context=@context 
                                    WHERE bid=@File_id;";
                using (SqlCommand command = new SqlCommand(sql))
                {
                    command.Parameters.AddWithValue("title", title);
                    command.Parameters.AddWithValue("subtitle", subtitle);
                    command.Parameters.AddWithValue("context", context);

                    command.Parameters.AddWithValue("bid", bid);              
                    m_db.ExecuteNonQuery(command);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }





        public bool DelBlog(int uid)
        {
            try
            {            
                    string sql = @"DELETE FROM blogs
                                    WHERE bid=@uid;";
                    using (SqlCommand command = new SqlCommand(sql))
                    {
                        command.Parameters.AddWithValue("uid", uid);
                        m_db.ExecuteNonQuery(command);
                    }
                    
                    return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
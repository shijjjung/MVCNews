﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MVCnews.DataSet.SQL
{
    public class MSSQL
    {
        /// <summary>
        /// sql connection
        /// </summary>
        private SqlConnection m_con = new SqlConnection();

        /// <summary>
        /// 建構子
        /// </summary>
        public MSSQL()
        {
            this.m_con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["NewsConnectionString"].ConnectionString;
        }

        /// <summary>
        /// 查詢
        /// </summary>
        /// <param name="_sSQL">sql查詢字串</param>
        /// <returns>回傳第一個資料列的第一個資料行</returns>
        public string ExecuteScalar(string _sql)
        {
            string result = string.Empty;

            try
            {
                SqlCommand cmd = new SqlCommand(_sql, this.m_con);

                this.m_con.Open();
                object temp = cmd.ExecuteScalar();
                result = temp != null ? Convert.ToString(temp) : string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (this.m_con.State != ConnectionState.Closed)
                    this.m_con.Close();
            }

            return result;
        }

        /// <summary>
        /// 查詢
        /// </summary>
        /// <param name="_sSQL">sql查詢字串</param>
        /// <returns>回傳第一個資料列的第一個資料行</returns>
        public string ExecuteScalar(SqlCommand _sqlCmd)
        {
            string result = string.Empty;

            try
            {
                _sqlCmd.Connection = this.m_con;

                this.m_con.Open();
                object temp = _sqlCmd.ExecuteScalar();
                result = temp != null ? Convert.ToString(temp) : string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (this.m_con.State != ConnectionState.Closed)
                    this.m_con.Close();
            }

            return result;
        }

        /// <summary>
        /// insert、update、delete
        /// </summary>
        /// <param name="_sSQL">SQL字串</param>
        /// <returns>受影響的資料列數目</returns>
        public int ExecuteNonQuery(string _sql)
        {
            int result = 0;

            try
            {
                SqlCommand cmd = new SqlCommand(_sql, this.m_con);

                this.m_con.Open();
                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (this.m_con.State != ConnectionState.Closed)
                    this.m_con.Close();
            }

            return result;
        }

        /// <summary>
        /// insert、update、delete
        /// </summary>
        /// <param name="_sqlCmd">SQL指令</param>
        /// <returns>受影響的資料列數目</returns>
        public int ExecuteNonQuery(SqlCommand _sqlCmd)
        {
            int result = 0;

            try
            {
                _sqlCmd.Connection = this.m_con;

                this.m_con.Open();
                result = _sqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (this.m_con.State != ConnectionState.Closed)
                    this.m_con.Close();
            }

            return result;
        }

        /// <summary>
        /// 查詢
        /// </summary>
        /// <param name="_sSQL">SQL字串</param>
        /// <returns>回傳查詢結果的DataTable</returns>
        public DataTable Fill(string _sql)
        {
            DataTable dt = new DataTable();

            try
            {
                using (SqlDataAdapter da = new SqlDataAdapter(_sql, this.m_con.ConnectionString))
                {
                    da.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        /// <summary>
        /// 查詢
        /// </summary>
        /// <param name="_sqlCmd">SQL指令</param>
        /// <returns>回傳查詢結果的DataTable</returns>
        public DataTable Fill(SqlCommand _sqlCmd)
        {
            _sqlCmd.Connection = this.m_con;

            DataTable dt = new DataTable();

            try
            {
                using (SqlDataAdapter da = new SqlDataAdapter(_sqlCmd))
                {
                    da.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
    }
}